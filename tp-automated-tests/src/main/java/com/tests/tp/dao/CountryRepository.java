package com.tests.tp.dao;

import com.tests.tp.dto.SearchCountryCriteria;
import com.tests.tp.model.Country;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface CountryRepository extends JpaRepository<Country, String>, JpaSpecificationExecutor<Country> {

    interface Specs {
        static Specification<Country> byCountryCriteria(SearchCountryCriteria searchCountryCriteria) {
            return ((root, query, builder) -> {
                query.distinct(true);
                List<Predicate> predicates = new ArrayList<>();
                if (null != searchCountryCriteria.getName()) {
                    predicates.add(builder.like(builder.upper(root.get("name")), searchCountryCriteria.getName().toUpperCase()));
                }

                if (null != searchCountryCriteria.getCityName()) {
                    var joinCity = root.join("cities", JoinType.LEFT);
                    predicates.add(builder.like(builder.upper(joinCity.get("name")), searchCountryCriteria.getCityName().toUpperCase()));
                }

                if(CollectionUtils.isEmpty(predicates)){
                    return builder.conjunction();
                }
                return builder.or(predicates.toArray(Predicate[]::new));
            });
        }
    }
}
