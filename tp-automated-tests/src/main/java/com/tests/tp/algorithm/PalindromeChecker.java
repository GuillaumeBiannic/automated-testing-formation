package com.tests.tp.algorithm;

public class PalindromeChecker {

    private PalindromeChecker() {
    }

    /**
     * Check if given word is palindrome
     * @param word word to check
     * @return true if palindrome, else otherwise
     */
    public static boolean isPalindrome(String word) {
        if (word == null || word.isEmpty() || word.length() == 1) {
            return false;
        }

        int start = 0;
        int end = word.length() - 1;

        while (start < end) {
            if (word.charAt(start) != word.charAt(end)) {
                return false;
            }
            start++;
            end--;
        }

        return true;
    }
}
