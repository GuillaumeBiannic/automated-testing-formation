package com.tests.tp.algorithm;

import java.util.List;

public class TemperatureHelper {

    /**
     * get closest temperature to 0
     *
     * @param temperatures values to check
     * @return 0 if no temperature is given, closest temperature of 0 otherwise
     */
    public Integer getClosestOfZero(List<Integer> temperatures) {
        if (null == temperatures) {
            throw new IllegalArgumentException("Argument temperatures cannot be null");
        }

        return temperatures.stream().reduce(this::compareTempClosestOfZero).orElse(0);
    }

    private int compareTempClosestOfZero(int temperature1, int temperature2) {
        if (Math.abs(temperature1) < Math.abs(temperature2) || (temperature1 > 0 && -temperature2 == temperature1)) {
            return temperature1;
        } else {
            return temperature2;
        }
    }
}

