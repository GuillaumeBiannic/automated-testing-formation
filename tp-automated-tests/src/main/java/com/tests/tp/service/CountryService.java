package com.tests.tp.service;


import com.tests.tp.dto.SearchCountryCriteria;
import com.tests.tp.dto.CountryDto;
import com.tests.tp.model.Country;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface CountryService {

  List<CountryDto> search(SearchCountryCriteria critere);

  CountryDto findOne(String code);

  @Transactional
  CountryDto create(CountryDto pays);

  @Transactional
  CountryDto update(String code, CountryDto pays);

  @Transactional
  void delete(String code);
}
