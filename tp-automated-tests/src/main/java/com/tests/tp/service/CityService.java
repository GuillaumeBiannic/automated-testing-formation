package com.tests.tp.service;

import com.tests.tp.dto.CityDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface CityService {

    List<CityDto> findAll();

    CityDto findOne(String code);

    @Transactional
    CityDto create(CityDto pays);

    @Transactional
    CityDto update(String code, CityDto pays);

    @Transactional
    void delete(String code);
}
