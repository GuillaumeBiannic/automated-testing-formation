package com.tests.tp.service.impl;

import com.tests.tp.dao.CountryRepository;
import com.tests.tp.dto.CountryDto;
import com.tests.tp.dto.SearchCountryCriteria;
import com.tests.tp.exception.DeleteDependencyException;
import com.tests.tp.exception.NotFoundEntityException;
import com.tests.tp.mapper.CountryMapper;
import com.tests.tp.model.Country;
import com.tests.tp.service.CountryService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

import static com.tests.tp.dao.CountryRepository.Specs.byCountryCriteria;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    private final CountryMapper countryMapper;

    public CountryServiceImpl(CountryRepository countryRepository, CountryMapper countryMapper) {
        this.countryRepository = countryRepository;
        this.countryMapper = countryMapper;
    }

    @Override
    public List<CountryDto> search(SearchCountryCriteria critere) {
        return countryRepository.findAll(byCountryCriteria(critere)).stream().map(countryMapper::toDto).toList();
    }

    @Override
    public CountryDto findOne(String code) {
        return countryMapper.toDto(findOneEntity(code));
    }

    @Override
    public CountryDto create(CountryDto pays) {
        Country entity = countryMapper.toEntity(pays);
        entity.setCreationDate(LocalDateTime.now());
        return countryMapper.toDto(countryRepository.save(entity));
    }

    @Override
    public CountryDto update(String code, CountryDto pays) {
        Country entity = findOneEntity(code);
        entity.setName(pays.getName());
        return countryMapper.toDto(countryRepository.save(entity));
    }

    @Override
    public void delete(String code) {
        Country country = findOneEntity(code);
        if (!CollectionUtils.isEmpty(country.getCities())) {
            throw new DeleteDependencyException(
                    String.format("Cannot delete country %s because one or more city referenced it", code));
        }
        countryRepository.deleteById(country.getCode());
    }

    private Country findOneEntity(String code) {
        return countryRepository.findById(code).orElseThrow(NotFoundEntityException::new);
    }
}
