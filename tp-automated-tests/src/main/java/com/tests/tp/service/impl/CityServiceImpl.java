package com.tests.tp.service.impl;

import com.tests.tp.dao.CityRepository;
import com.tests.tp.dao.CountryRepository;
import com.tests.tp.dto.CityDto;
import com.tests.tp.exception.NotFoundEntityException;
import com.tests.tp.mapper.CityMapper;
import com.tests.tp.model.City;
import com.tests.tp.service.CityService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    private final CityMapper cityMapper;

    private final CountryRepository countryRepository;

    public CityServiceImpl(CityRepository cityRepository, CityMapper cityMapper, CountryRepository countryRepository) {
        this.cityRepository = cityRepository;
        this.cityMapper = cityMapper;
        this.countryRepository = countryRepository;
    }

    @Override
    public List<CityDto> findAll() {
        return cityRepository.findAll().stream().map(cityMapper::toDto).toList();
    }

    @Override
    public CityDto findOne(String code) {
        return cityMapper.toDto(findOneEntity(code));
    }

    @Override
    public CityDto create(CityDto city) {
        if(null == city.getCountryCode()){
            throw new IllegalArgumentException("Country is required to create a city");
        }
        City entity = cityMapper.toEntity(city);
        return cityMapper.toDto(cityRepository.save(entity));
    }

    @Override
    public CityDto update(String code, CityDto cityDto) {
        City entity = findOneEntity(code);
        entity.setName(cityDto.getName());
        if (null != cityDto.getCountryCode() &&
                !cityDto.getCountryCode().equals(entity.getCountry().getCode())) {
            entity.setCountry(countryRepository.findById(cityDto.getCountryCode())
                    .orElseThrow(NotFoundEntityException::new));
        }
        return cityMapper.toDto(cityRepository.save(entity));
    }

    @Override
    public void delete(String code) {
        City city = findOneEntity(code);
        cityRepository.deleteById(city.getCode());
    }

    private City findOneEntity(String code) {
        return cityRepository.findById(code).orElseThrow(NotFoundEntityException::new);
    }
}
