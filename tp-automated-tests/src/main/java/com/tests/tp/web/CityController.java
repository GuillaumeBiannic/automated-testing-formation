package com.tests.tp.web;

import com.tests.tp.dto.CityDto;
import com.tests.tp.service.CityService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/cities")
public class CityController {

    private final CityService cityService;

    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping
    List<CityDto> findAll() {
        return cityService.findAll();
    }

    @GetMapping(value = "/{code}")
    CityDto findOne(@PathVariable("code") String code) {
        return cityService.findOne(code);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    CityDto create(@RequestBody @Valid CityDto ville) {
        return cityService.create(ville);
    }

    @PutMapping(value = "/{code}")
    CityDto update(@PathVariable("code") String code, @RequestBody @Valid CityDto ville) {
        return cityService.update(code, ville);
    }

    @DeleteMapping(value = "/{code}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable String code) {
        cityService.delete(code);
    }
}
