package com.tests.tp.web.exception.handler;

import com.tests.tp.exception.DeleteDependencyException;
import com.tests.tp.exception.NotFoundEntityException;
import com.tests.tp.web.exception.response.ApiError;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(NotFoundEntityException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiError handleEntityNotFoundException(HttpServletRequest request, NotFoundEntityException ex) {
        return new ApiError(LocalDateTime.now(),
                request.getRequestURI(),
                HttpStatus.NOT_FOUND.value(),
                "NOT_FOUND",
                null != ex.getMessage() ? ex.getMessage() : "Entity not found");
    }

    @ExceptionHandler(DeleteDependencyException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ApiError handleDeleteDependencyException(HttpServletRequest request, DeleteDependencyException ex) {
        return new ApiError(LocalDateTime.now(),
                request.getRequestURI(),
                HttpStatus.CONFLICT.value(),
                "CONFLICT",
                null != ex.getMessage() ? ex.getMessage() :
                        "Could not delete entity because of pending dependency");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleIllegalArgumentException(HttpServletRequest request, IllegalArgumentException ex) {
        return new ApiError(LocalDateTime.now(),
                request.getRequestURI(),
                HttpStatus.BAD_REQUEST.value(),
                "BAD_REQUEST",
                null != ex.getMessage() ? ex.getMessage() :
                        "Request could not be fulfill, please verify your data");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiError handleMethodArgumentNotValidException(HttpServletRequest request, MethodArgumentNotValidException ex) {
        return new ApiError(LocalDateTime.now(),
                request.getRequestURI(),
                HttpStatus.BAD_REQUEST.value(),
                "BAD_REQUEST",
                "Invalid send data, please verify your data");
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiError handleRuntineException(HttpServletRequest request) {
        return new ApiError(LocalDateTime.now(),
                request.getRequestURI(),
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "INTERNAL_ERROR",
                "Something went wrong, please contact support");
    }
}
