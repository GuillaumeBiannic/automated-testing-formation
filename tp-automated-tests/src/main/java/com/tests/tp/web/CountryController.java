package com.tests.tp.web;

import com.tests.tp.dto.SearchCountryCriteria;
import com.tests.tp.dto.CountryDto;
import com.tests.tp.service.CountryService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/countries")
public class CountryController {

  private final CountryService countryService;

  @Autowired
  CountryController(CountryService countryService) {
    this.countryService = countryService;
  }

  @GetMapping
  List<CountryDto> search(SearchCountryCriteria critere) {
    return countryService.search(critere);
  }

  @GetMapping(value = "/{code}")
  CountryDto findOne(@PathVariable("code") String code) {
    return countryService.findOne(code);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  CountryDto create(@RequestBody @Valid CountryDto pays) {
    return countryService.create(pays);
  }

  @PutMapping(value = "/{code}")
  CountryDto update(@PathVariable("code") String code, @RequestBody @Valid CountryDto pays) {
    return countryService.update(code, pays);
  }

  @DeleteMapping(value = "/{code}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void delete(@PathVariable String code) {
    countryService.delete(code);
  }
}
