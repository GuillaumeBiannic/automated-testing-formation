package com.tests.tp.web.exception.response;

import java.time.LocalDateTime;


public record ApiError(LocalDateTime date,
                       String url,
                       Integer httpCode,
                       String code,
                       String message) {}
