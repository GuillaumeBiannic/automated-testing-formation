package com.tests.tp.mapper;

import com.tests.tp.dto.CountryDto;
import com.tests.tp.model.Country;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CityMapper.class})
public interface CountryMapper {


    @Mapping(target = "creationDate", ignore = true)
    Country toEntity(CountryDto countryDto);

    CountryDto toDto(Country country);

}
