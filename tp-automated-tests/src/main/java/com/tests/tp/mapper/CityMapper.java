package com.tests.tp.mapper;

import com.tests.tp.dto.CityDto;
import com.tests.tp.model.City;
import com.tests.tp.model.Country;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring", uses = {CountryMapper.class})
public interface CityMapper {

    @Mapping(target = "country", source = "countryCode", qualifiedByName = "definedCountry")
    City toEntity(CityDto cityDto);

    @Named("definedCountry")
    default Country definedCountry(String countryCode) {
        Country country = new Country();
        country.setCode(countryCode);
        return country;
    }

    @Mapping(target = "countryCode", source = "country", qualifiedByName = "definedCountryCode")
    CityDto toDto(City city);

    @Named("definedCountryCode")
    default String definedCountryCode(Country country) {
        return country.getCode();
    }

}
