package com.tests.tp.dto;

import jakarta.validation.constraints.Pattern;

import java.util.Objects;

public class CityDto {

    @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Code can't have special characters")
    private String code;

    @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Name can't have special characters")
    private String name;

    private String countryCode;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityDto cityDto = (CityDto) o;
        return Objects.equals(code, cityDto.code) && Objects.equals(name, cityDto.name) && Objects.equals(countryCode, cityDto.countryCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, countryCode);
    }
}
