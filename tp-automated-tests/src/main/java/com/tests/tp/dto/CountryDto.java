package com.tests.tp.dto;

import jakarta.validation.constraints.Pattern;

import java.util.Objects;
import java.util.Set;

public class CountryDto {

    @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Code can't have special characters")
    private String code;

    @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Title can't have special characters")
    private String name;

    private Set<CityDto> cities;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CityDto> getCities() {
        return cities;
    }

    public void setCities(Set<CityDto> cities) {
        this.cities = cities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryDto countryDto = (CountryDto) o;
        return Objects.equals(code, countryDto.code) && Objects.equals(name, countryDto.name) && Objects.equals(cities, countryDto.cities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, cities);
    }
}
