package com.tests.tp.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "tp_city")
public class City {

    @Id
    @Column(name = "city_code")
    private String code;

    @Column(name = "city_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "country_code", referencedColumnName = "country_code", nullable = false)
    private Country country;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String libelle) {
        this.name = libelle;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(code, city.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
