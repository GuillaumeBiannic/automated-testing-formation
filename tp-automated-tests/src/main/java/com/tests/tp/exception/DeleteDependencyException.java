package com.tests.tp.exception;

public class DeleteDependencyException extends RuntimeException {

    public DeleteDependencyException(String message) {
        super(message);
    }
}
